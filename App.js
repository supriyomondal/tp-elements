
import React from 'react';
import { StyleSheet, View} from 'react-native';
// import GameNews from './Component/GameNews';
import LiveGame from './Component/LiveGames';
// import UpcomingGame from './Component/UpcomingGame';
// import FutureCards from "./Component/CoinManagement";
// import Transactions from "./Component/Transaction";
export default function App() {
  return (
    <View style={styles.container}>
      <LiveGame />
      {/* <UpcomingGame />
      <GameNews />
      <FutureCards />
      <Transactions /> */}

    </View>
  );
}

const styles = StyleSheet.create({
  
  container: {  
    flex: 1,
    backgroundColor: '#F5F3F3',
    alignItems: 'center',
    justifyContent: 'center',
    
  },

});
