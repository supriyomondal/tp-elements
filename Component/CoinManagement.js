import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { IconButton} from 'react-native-paper';


function FutureCards() {
    return (
    <View style={styles.container}>
        <View style ={styles.container__next}> 
            <View style ={styles.elements__top1}>
                <View style={styles.element__feedContainer}>
                <Text style={styles.element__text}>Featured</Text>
                </View>
                <View style={styles.button__Icon}>
                    <View style={styles.injuryButton}>
                        <Text style={styles.injuryText}>Injured</Text>
                    </View>
                    <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                </View>
            </View>
            <View style={styles.injuredNewsBox}>
                <Text style={styles.headline}>Head line of a news which is featured on top of this page</Text>
                <View style={styles.source}>
                    <View style={styles.sourceName}> 
                        <Text style={styles.textSource}>Source name</Text>
                    </View>
                    <View style={styles.sourceTime}>
                        <Text style={styles.textSource}>Date of news</Text>
                    </View>
                </View>
            </View>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
    textSource:{
        color:"green"
    },
    sourceTime:{
    flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingTop:6,
    paddingBottom:6,
    paddingLeft:6,
    },

    sourceName:{
    flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    paddingTop:6,
    paddingBottom:6,
    paddingRight:6,
    borderRightWidth:3,
    borderRightColor:"green"
    },
    source:{
        flexDirection:"row",
        padding:10,
    },
    headline:{
        padding:10,
        fontSize:26,
        color:"white"
    },
    injuredNewsBox:{
    width:470,
    height:350,
    margin:"auto", 
    padding:2,
    marginBottom:20,
    marginTop:15,
    borderRadius:20,
    backgroundColor: "#000000",
    backgroundImage: "linear-gradient(to top, #ffffff  0%,  #707070 98%)",
    },
    injuryText:{
    marginLeft:5,
    fontSize:15,
    fontWeight:400,
    color:'white'
    },
    button__Icon:{
        flexDirection:'row',
        justifyContent: 'space-evenly',
        alignItems:"center"
    },
    injuryButton:{
    flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor:"coral",
    paddingTop:6,
    paddingBottom:6,
    paddingLeft:30,
    paddingRight:30,
    borderRadius:10
    },
   container__next:{
    padding:20,
    backgroundColor:"white",
    borderRadius:40,
    marginTop:30,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 4.84,
  elevation: 5,
  },
  elements__top1:{
    flexDirection:'row',
    width:500, 
    padding:8,
  },
  element__feedContainer:{
      flex:1,
    flexDirection:'row',
    justifyContent:"flex-start",
    alignItems:"center"
  },
  element__text:{
    fontWeight:'bold',
    fontSize:24,
    marginLeft:7,
    color:"rgba(0, 0, 0, 0.5)"
  },
 
container: {  
    flex: 1,
    backgroundColor: '#F5F3F3',
    alignItems: 'center',
    justifyContent: 'center',
    
  },

});
       
export default FutureCards
