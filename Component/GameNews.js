import React from 'react';
import { Image, StyleSheet, Text, View} from 'react-native';
import { Button } from 'react-native-elements';
import { IconButton} from 'react-native-paper';
export default function GameNews() {
  return (
    <View style={styles.container}>
          <View style ={styles.container__next}> 
        <View style ={styles.elements__top1}>
            <View style={styles.element__feedContainer}>
              <Text style={styles.element__text}>Latest News</Text>
            </View>
        </View>
        <View style ={styles.elements__bottom1}>
            <View style={styles.element__containerBig}>
              <View style={styles.element__wrap}>
                  <View style={styles.element__newsContainer}> 
                    {/* Image goes here  */}
                  </View>
                  <View style={styles.element__newsFeed}>
                    <View style={styles.element__newsInfo}>
                      <Text style={styles.newsText1}>Date of news </Text>
                      <Text style={styles.newsText2}>Topic Name</Text>
                    </View>
                    <Text style={styles.newsTextinfo}>Sapient in explicabo nihil eveniet<br></br>quis dolores quas.</Text>
                    <View style={styles.newsSettings}>
                      <Text style={styles.newsText3}>Source name</Text>
                      <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>  
              </View>
            </View>
        </View>
        <View style ={styles.elements__bottom1}>
            <View style={styles.element__containerBig}>
              <View style={styles.element__wrap}>
                  <View style={styles.element__newsContainer}> 
                    {/* Image goes here  */}
                  </View>
                  <View style={styles.element__newsFeed}>
                    <View style={styles.element__newsInfo}>
                      <Text style={styles.newsText1}>Date of news </Text>
                      <Text style={styles.newsText2}>Topic Name</Text>
                    </View>
                    <Text style={styles.newsTextinfo}>Sapient in explicabo nihil eveniet<br></br>quis dolores quas.</Text>
                    <View style={styles.newsSettings}>
                      <Text style={styles.newsText3}>Source name</Text>
                      <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>  
              </View>
            </View>
        </View>
        <View style ={styles.elements__bottom1}>
            <View style={styles.element__containerBig}>
              <View style={styles.element__wrap}>
                  <View style={styles.element__newsContainer}> 
                    {/* Image goes here  */}
                  </View>
                  <View style={styles.element__newsFeed}>
                    <View style={styles.element__newsInfo}>
                      <Text style={styles.newsText1}>Date of news </Text>
                      <Text style={styles.newsText2}>Topic Name</Text>
                    </View>
                    <Text style={styles.newsTextinfo}>Sapient in explicabo nihil eveniet<br></br>quis dolores quas.</Text>
                    <View style={styles.newsSettings}>
                      <Text style={styles.newsText3}>Source name</Text>
                      <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>  
              </View>
            </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container__next:{
    padding:20,
    backgroundColor:"white",
    borderRadius:40,
    marginTop:30,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 4.84,
  elevation: 5,
  },
  elements__top1:{
    flexDirection:'row',
    width:500, 
    padding:8,
  },
  element__feedContainer:{
    flexDirection:'row',
    justifyContent:"flex-end",
    alignItems:"center"
  },
  element__text:{
    fontWeight:'bold',
    fontSize:24,
    marginLeft:7
  },
  elements__bottom1:{
    width:500, 
    padding:2,
    borderBottomWidth:2,
    borderBottomColor:"lightgrey"
  },
  newsSettings:{
    marginTop:30,
    flexDirection:"row",
    justifyContent:'space-evenly',
    alignItems:'center'
  },
  newsTextinfo:{
    marginTop:10,
    fontSize:21,
    fontWeight:"500",
  },
  newsText3:{
    fontSize:15,
    fontWeight:"500",
    color:"#046307",
    flex:1
  },
  newsText2:{
    fontSize:15,
    fontWeight:"400",
    color:"red"
  },
  newsText1:{
    fontSize:15,
    fontWeight:"400",
    color:"grey",
    flex:1,
  },
  element__newsInfo:{
    flexDirection:"row",
    justifyContent:"space-evenly",
    alignItems:"center"
  },
  element__newsFeed:{
    flexDirection:"column",
    marginLeft:20
  },
  element__wrap:{
    flexDirection:"row",
  },
  element__newsContainer:{
    width: 150,
    height: 150,
    backgroundColor:"lightgrey",
    borderRadius:15
  },

 element__containerBig:{
  paddingTop:20,
  paddingBottom:20,
  flexDirection:"row",
 },
container: {  
    flex: 1,
    backgroundColor: '#F5F3F3',
    alignItems: 'center',
    justifyContent: 'center',
    
  },

});