import React from 'react';
import { Image, StyleSheet, Text, View} from 'react-native';
import { Button } from 'react-native-elements';
import { IconButton} from 'react-native-paper';
import  * as ButtonConfig from '../styles/buttons';
import  * as ColorConfig from '../styles/colors';
export default function LiveGame() {
  return (
    <View style={styles.container}>
      <View style ={styles.container__whole}> 
        <View style ={styles.elements__top}>
            <View style={styles.element__feedContainer}>
              <View  style={styles.element__colorBox}>
                <View style={styles.element__circle}></View>
                  <Text style={styles.element__liveText}>LIVE</Text>
              </View>
              <Text style={styles.element__text}>Matches</Text>
            </View>
            <View style={styles.element__button}>
              <Button title="View All"  type="clear" />
            </View>
        </View>
        <View style ={styles.elements__bottom}>
          <Text style ={styles.element_leagueText}>Name of the league</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>Australia</Text>
                  </View>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>India</Text>
                  </View>
              </View>
              <View style={styles.element__score}>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>138-2 (AUS)</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="bell-ring" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>21.5 Overs</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="currency-inr" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
              </View>
            </View>
        </View>
      
        
      
      
      </View>

    </View>
  );
}

const styles = StyleSheet.create({
  
  container__whole:{
    borderWidth:4,
    padding:20,
    borderColor:"#ff5050",
    borderRadius:40,
    backgroundColor:"white"
  },


  element__iconContainer:{
    backgroundColor:"rgba(0, 0, 0, 0.1)",
    marginLeft:10,
    borderRadius:999,
    width:50,
    height:50
  },


  element__country:{
    flex:1,
    flexDirection:"column",
    borderRightWidth:2,
    borderRightColor:"lightgrey",

  },
  element__score:{
    flex:1,
    flexDirection:"column",
    alignItems:"center",
    justifyContent:"space-evenly",

  },
  element__countryContainer2:{
    width:230,
    flexDirection:"row",
    justifyContent:"flex-end",
    alignItems:"center",
    marginTop:10
  },
  element__countryContainer:{
    flexDirection:"row",
    justifyContent:"flex-start",
    alignItems:"center",
    marginTop:10
  },
  element__countrytext1:{
    fontSize:20,
    marginLeft:20,
   
    width:"100%",
    color:"rgba(0, 0, 0, 0.8)"
},
 element__countrytext:{
   fontSize:20,
   marginLeft:20,
   width:"100%",
   color:"rgba(0, 0, 0, 0.5)"
 },
 element__containerBig:{
  paddingTop:20,
  paddingBottom:20,
  flexDirection:"row",
 },
  tinyLogo: {
    width: 50,
    height: 50,
  },
  element_leagueText:{
    fontSize:20,
    fontWeight:"light",
    color:"rgba(0, 0, 0, 0.4)",
  },

  elements__bottom:{
    marginTop:12,
    width:500, 
    padding:8,
    borderBottomWidth:2,
    borderBottomColor:"lightgrey"
  },



  elements__top:{
    flexDirection:'row',
    width:500, 
    padding:8,
    borderBottomWidth:2,
    borderBottomColor:"lightgrey"
  },
  element__button:{
   flex:3,
    flexDirection:'row',
    justifyContent:'flex-end'
  },
  element__feedContainer:{
    flexDirection:'row',
    justifyContent:"flex-end",
    alignItems:"center"
  },

  element__text:{
    fontWeight:'bold',
    fontSize:24,
    marginLeft:7
  },
  element__liveText:{
    marginLeft:5,
    fontWeight:"bold",
    color:'white'
  },
  element__colorBox:{
    ...ButtonConfig.bar.secondary, 
    flexDirection:'row',
    justifyContent: 'space-evenly',
    backgroundColor:"rgba(0, 0, 0, 0.8)",
  },
  element__circle:{
    backgroundColor: '#FF4500',
    borderRadius: 10,
    width: '10px',
    height: '10px'
  },
  container: {  
    flex: 1,
    backgroundColor: '#F5F3F3',
    alignItems: 'center',
    justifyContent: 'center',
    
  },

});
