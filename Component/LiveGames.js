import React from 'react';
import { Button, Image, StyleSheet, Text, View} from 'react-native';
import { colors } from 'react-native-elements';

import {Typography, Sizing, Outlines, Colors, Buttons} from '../styles/index';

export default function LiveGame() {
  return (
    <View style={styles.container}>
        <View style={styles.Layout__Top}>
            <View style={styles.LiveNav}>
                <View style={styles.LiveButton}>
                    <View style={styles.roundLive}></View>
                    <Text style={styles.liveButtonTxt}>LIVE</Text>
                </View>
                {/* Text goes here */}
                {/* Button goes here */}
            </View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
  
    container: {  
        ...Sizing.screen,  
        ...Sizing.padding.a1,
        backgroundColor: Colors.neutral.white,
        borderColor: Colors.danger.s400,
        borderWidth: Outlines.borderWidth.base,
        borderRadius: Outlines.borderRadius.base,~
    },


// Live Nav elements

    liveButtonTxt:{
        color: Colors.neutral.white,
        ...Typography.fontWeight.semibold
    },
    LiveButton:{
        flexDirection:"row",
        ...Buttons.bar.secondary,
        backgroundColor: Colors.neutral.s900
    },

    roundLive:{
        ...Buttons.rounded,
        backgroundColor: Colors.danger.s400,
        height: Sizing.layout.x10,
        width: Sizing.layout.x10,
    }

});
