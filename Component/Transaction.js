import React from 'react';
import { StyleSheet, Text, View} from 'react-native';
function Transactions() {
  return (
     <View style={styles.container}>

      <View style ={styles.container__next}> 
        <View style ={styles.elements__top1}>
            <View style={styles.element__feedContainer}>
              <View>
                <Text style={styles.element__transactionText}>Recent Transactions</Text>
              </View>
            </View>

        </View>
        <View style ={styles.elements__bottom}>
          <Text style ={styles.element_dateOfTransaction}>[Date of transaction]</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <View style={styles.tinyLogo}></View>
                    <View style={styles.transaction__container}>
                        <View style={styles.reminder1}>
                            <View>
                                <Text style={styles.element__countrytext1}>[Where coins where used]</Text>
                                <Text style={styles.time}>9:36 AM</Text>
                            </View>
                        </View>
                        <View style={styles.reminder2}>
                            <View>
                                <Text style={styles.coinRemaining}>-20 Coins</Text>
                                <Text style={styles.coinDetail}>Closing balance: 350 Coins</Text>
                            </View>
                        </View>
                    </View>
                  </View>
              </View>

            </View>
        </View>

        <View style ={styles.elements__bottom1}>
          <Text style ={styles.element_dateOfTransaction}>[Date of transaction]</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <View style={styles.tinyLogo}></View>
                    <View style={styles.transaction__containerFinal}>
                        <View style={styles.reminder1}>
                            <View>
                                <Text style={styles.element__countrytext1}>[Where coins where used]</Text>
                                <Text style={styles.time}>9:36 AM</Text>
                            </View>
                        </View>
                        <View style={styles.reminder2}>
                            <View>
                                <Text style={styles.coinRemaining2}>+20 Coins</Text>
                                <Text style={styles.coinDetail}>Closing balance: 350 Coins</Text>
                            </View>
                        </View>
                    </View>
                  </View>
              </View>

            </View>
        </View>

        <View style ={styles.elements__bottom}>
          <Text style ={styles.element_dateOfTransaction}>[Date of transaction]</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <View style={styles.tinyLogo}></View>
                    <View style={styles.transaction__container}>
                        <View style={styles.reminder1}>
                            <View>
                                <Text style={styles.element__countrytext1}>[Where coins where used]</Text>
                                <Text style={styles.time}>9:36 AM</Text>
                            </View>
                        </View>
                        <View style={styles.reminder2}>
                            <View>
                                <Text style={styles.coinRemaining}>-20 Coins</Text>
                                <Text style={styles.coinDetail}>Closing balance: 350 Coins</Text>
                            </View>
                        </View>
                    </View>
                  </View>
              </View>

            </View>
        </View>

        <View style ={styles.elements__bottom1}>
          <Text style ={styles.element_dateOfTransaction}>[Date of transaction]</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <View style={styles.tinyLogo}></View>
                    <View style={styles.transaction__containerFinal}>
                        <View style={styles.reminder1}>
                            <View>
                                <Text style={styles.element__countrytext1}>[Where coins where used]</Text>
                                <Text style={styles.time}>9:36 AM</Text>
                            </View>
                        </View>
                        <View style={styles.reminder2}>
                            <View>
                                <Text style={styles.coinRemaining}>-20 Coins</Text>
                                <Text style={styles.coinDetail}>Closing balance: 350 Coins</Text>
                            </View>
                        </View>
                    </View>
                  </View>
              </View>

            </View>
        </View>

        

      </View>
</View>
  );
}
const styles = StyleSheet.create({
    time:{
        fontSize:15,
        color:"rgba(0, 0, 0, 0.3)",
        paddingTop:3,
    },
    coinDetail:{
        fontSize:15,
        paddingTop:3,
        color:"rgba(0, 0, 0, 0.3)"
    },
    coinRemaining2:{
        fontSize:24,
        alignSelf:'flex-end',
        color:'lightgreen'
    },
    coinRemaining:{
        fontSize:24,
        alignSelf:'flex-end',
        color:"red"

    },
    reminder1:{
        marginBottom:10,
        justifyContent:"center",
    },
    reminder2:{
        marginBottom:10,
        flex:1,
        alignItems:"center",
        flexDirection:"row",
        justifyContent:"flex-end",
    },
    transaction__containerFinal:{
        flexDirection:"row",
        flex:1,
    },
    transaction__container:{
        flexDirection:"row",
        flex:1,
        borderBottomWidth:2,
        borderBottomColor:"rgba(0, 0, 0, 0.2)"
    },
  elements__top1:{
    flexDirection:'row',
    width:500, 
    padding:8,
  },
  container__next:{
    padding:20,
    backgroundColor:"white",
    borderRadius:40,
    marginTop:30,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 4.84,
  elevation: 5,
  },
  element__transactionText:{
    fontWeight:'bold',
    fontSize:24,
    marginBottom:15,
    marginTop:15
  },
    element__text:{
    fontWeight:'bold',
    fontSize:24,
    marginLeft:7
  },
  elements__bottom1:{
    marginTop:8,
    width:500, 
    padding:8,
    borderBottomWidth:2,
    borderBottomColor:"lightgrey"
  },
   elements__bottom:{
    marginTop:12,
    width:500, 
    padding:8,
  },
  element_dateOfTransaction:{
    fontSize:20,
    fontWeight:"500",
    color:"lightgreen",
  },
  element__containerBig:{
  paddingTop:20,
  paddingBottom:20,
  flexDirection:"row",
 },
  tinyLogo: {
    marginRight:10,
    width: 50,
    height: 50,
    backgroundColor:'rgba(0, 0, 0, 0.5)',
    borderRadius:999
  },
element__country:{
    flex:1,
    flexDirection:"column",

  },

  element__countryContainer:{
    flexDirection:"row",
    justifyContent:"flex-start",
    alignItems:"center",
    marginTop:10
  },
  element__countrytext1:{
    fontSize:20,
    width:"100%",
    color:"rgba(0, 0, 0, 0.8)"
},
  element__feedContainer:{
    width:500,
    flexDirection:'row',
    justifyContent:"flex-start",
    alignItems:"center",
    borderBottomWidth:2,
    borderBottomColor:"rgba(0, 0, 0, 0.2)"
  },

 container: {  
    flex: 1,
    backgroundColor: '#F5F3F3',
    alignItems: 'center',
    justifyContent: 'center',
    
  },

});

export default Transactions