import React from 'react';
import { Image, StyleSheet, Text, View} from 'react-native';
import { Button } from 'react-native-elements';
import { IconButton} from 'react-native-paper';
export default function UpcomingGame() {
  return (
     <View style={styles.container}>

      <View style ={styles.container__next}> 
        <View style ={styles.elements__top1}>
            <View style={styles.element__feedContainer}>
              <View>
                <Text style={styles.element__Upcoming}>Upcoming</Text>
              </View>
              <Text style={styles.element__text}>Matches</Text>
            </View>
            <View style={styles.element__button}>
              <Button title="View All"  type="clear" />
            </View>
        </View>
        <View style ={styles.elements__bottom}>
          <Text style ={styles.element_leagueText}>Name of the league</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>Australia</Text>
                  </View>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>India</Text>
                  </View>
              </View>
              <View style={styles.element__score}>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>30 July</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="bell-ring" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>10:30 AM</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
              </View>
            </View>
        </View>
        <View style ={styles.elements__bottom}>
          <Text style ={styles.element_leagueText}>Name of the league</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>Australia</Text>
                  </View>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>India</Text>
                  </View>
              </View>
              <View style={styles.element__score}>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>30 July</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="bell-ring" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>10:30 AM</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
              </View>
            </View>
        </View>
        <View style ={styles.elements__bottom}>
          <Text style ={styles.element_leagueText}>Name of the league</Text>
            <View style={styles.element__containerBig}>
              <View style={styles.element__country}>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>Australia</Text>
                  </View>
                  <View style={styles.element__countryContainer}> 
                    <Image style={styles.tinyLogo} source={{uri:'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'}}/>
                    <Text style={styles.element__countrytext1}>India</Text>
                  </View>
              </View>
              <View style={styles.element__score}>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>30 July</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="bell-ring" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
                  <View style={styles.element__countryContainer2}> 
                    <Text style={styles.element__countrytext}>10:30 AM</Text>
                    <View style={styles.element__iconContainer}>
                      <IconButton icon="share-variant" color="rgba(0, 0, 0, 0.6)" size={25} />
                    </View>
                  </View>
              </View>
            </View>
        </View>  
      </View>
</View>
  );
}
const styles = StyleSheet.create({
  elements__top1:{
    flexDirection:'row',
    width:500, 
    padding:8,
  },
  container__next:{
    padding:20,
    backgroundColor:"white",
    borderRadius:40,
    marginTop:30,
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 2,
  },
  shadowOpacity: 0.25,
  shadowRadius: 4.84,
  elevation: 5,
  },
  element__Upcoming:{
    fontWeight:'bold',
    fontSize:24,
  },
    element__text:{
    fontWeight:'bold',
    fontSize:24,
    marginLeft:7
  },
  element__button:{
   flex:3,
    flexDirection:'row',
    justifyContent:'flex-end'
  },
   elements__bottom:{
    marginTop:12,
    width:500, 
    padding:8,
    borderBottomWidth:2,
    borderBottomColor:"lightgrey"
  },
  element_leagueText:{
    fontSize:20,
    fontWeight:"200",
    color:"rgba(0, 0, 0, 0.4)",
  },
  element__containerBig:{
  paddingTop:20,
  paddingBottom:20,
  flexDirection:"row",
 },
  tinyLogo: {
    width: 50,
    height: 50,
  },
element__country:{
    flex:1,
    flexDirection:"column",
    borderRightWidth:2,
    borderRightColor:"lightgrey",

  },
  element__countryContainer2:{
    width:230,
    flexDirection:"row",
    justifyContent:"flex-end",
    alignItems:"center",
    marginTop:10
  },
  element__countryContainer:{
    flexDirection:"row",
    justifyContent:"flex-start",
    alignItems:"center",
    marginTop:10
  },
  element__countrytext1:{
    fontSize:20,
    marginLeft:20,
    width:"100%",
    color:"rgba(0, 0, 0, 0.8)"
},
  element__feedContainer:{
    flexDirection:'row',
    justifyContent:"flex-end",
    alignItems:"center"
  },
  element__iconContainer:{
    backgroundColor:"rgba(0, 0, 0, 0.1)",
    marginLeft:10,
    borderRadius:999,
    width:50,
    height:50
  },
  element__countrytext:{
   fontSize:20,
   marginLeft:20,
   width:"100%",
   color:"rgba(0, 0, 0, 0.5)"
 },
element__score:{
    flex:1,
    flexDirection:"column",
    alignItems:"center",
    justifyContent:"space-evenly",

  },

 container: {  
    flex: 1,
    backgroundColor: '#F5F3F3',
    alignItems: 'center',
    justifyContent: 'center',
    
  },

});